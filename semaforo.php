<?php

	
	
	
	
		
	




	
	
	/* 
	
	-----------------------------
	
	Semáforo PHP
		
	Javier Martínez
	
	Versión 17 / 05 / 2020
	
	
	
	Control de concurrencia.
	Este programa permite 
	* Tomar decisiones en caso de concurrencia.
	* Establecer límites de tiempo.
	* Establecer límites de accesos.
	* Establecer semáforos personalizados.
	
	Con este programa puedes
	* Bloquear el acceso de múltiples usuarios a un segmento de código.
	* Detener inundaciones o intentos de accesos.
		
	
	
	
	Sitio:
	https://javim.000webhostapp.com/
	
	Códigos:
	https://gitlab.com/javipc/
	
	Basado en:
	https://www.microteching.com/php/concurrencia-bloqueo-de-codigo-y-semaforos-en-php
	
	
	proyecto que utiliza este programa
	
	https://t.me/RelojBot
	

	

	-----------------------------
	
		


	
	
	
	
	
	
	

### Crea un semáforo.
	
$semaforo = new Semaforo ();

$semaforo = new Semaforo ('usuario1234');





### Directorio de archivos.
$semaforo->ruta = '';


### Cambia la clave del semáforo.
Esto permite usar un semáforo personalizado.
Por ejemplo un semáforo por usuario, IP, actividad, etc.
$semaforo->clave = 'usuario1234';
	
	
	
	
	
## Concurrencia

### Bloquea el acceso concurrente.
Si otra conexión desea acceder suspende su ejecución y queda a la espera de que se libere el bloqueo.
$semaforo->bloquear ();

### Quita el bloqueo.	
$semaforo->desbloquear();




## Semáforo

### Segundos en los que expirará el semáforo.	
$semaforo->duracion = 10; 	


### Consulta si está activo (Bloqueado).
Devolverá "true" si:
* La consulta no supera el límite del contador.
* La consulta no supera los segundos establecidos.
if ($semaforo->activo())	

### Consulta si no está activo.
if ($semaforo->inactivo())	



### Enciende el semáforo.
$semaforo->encender ();

### Apaga el semáforo. 
Es de uso interno. No debería usarse.
$semaforo->apagar ();




## Otros métodos 

### Contador
cantidad de veces que permite pasar el semáforo antes de activarse.
$semaforo->contador (5);


### Apaga y vuelve a encender.
$semaforo->reencender();




	
	
## Ejemplos
	
	
	
	


###  Ejemplo Concurrencia 

Requiere dos instancias ejecutando la misma página al mismo tiempo.
	
	
	require_once ('semaforo.php');

	$semaforo = new Semaforo ();	
	$semaforo->ruta = '';
	

	echo  'Inicio del programa: ' 
		. date('Y-m-d H:i:s') 
		.'<br>' . PHP_EOL;
		
	$semaforo->bloquear ();
	
	echo  'Inicio del código protegido: ' 
		. date('Y-m-d H:i:s') 
		.'<br>' . PHP_EOL;
		
	sleep (5);
	$semaforo->desbloquear();
	
	echo  'Fin del programa: ' 
		. date('Y-m-d H:i:s') 
		.'<br>' . PHP_EOL;
	









### Ejemplo temporizador

Permite una conexión cada 5 segundos.
Se debe actualizar a página para ver el funcionamiento.
	
	require_once ('semaforo.php');
	
	$semaforo = new Semaforo ();
	$semaforo->duracion = 5;
	$semaforo->ruta = '';
	

	echo  'Es la hora: ' . date('Y-m-d H:i:s') .'<br>' . PHP_EOL;
	
	if ($semaforo->activo()) {
		echo '<STRONG> ¡BLOQUEADO! </STRONG> <BR>' . PHP_EOL;	
		echo 'hora límite: ' . $semaforo->limite .'<br>' . PHP_EOL;		
	}
		
	$semaforo->encender ();
	
	echo 'Límite del semáforo: ' . $semaforo->limite .'<br>' . PHP_EOL;
	

	
	



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
### Ejemplo con contador
Se debe actualizar la página para ver el funcionamiento.
	

	require_once ('semaforo.php');
	
	$semaforo = new Semaforo ();
	$semaforo->duracion = 10;
	$semaforo->ruta = '';
	$semaforo->contador (5);

	echo  'Es la hora: ' . date('Y-m-d H:i:s') .'<br>' . PHP_EOL;
	
	if ($semaforo->activo()) {
		echo '<STRONG> ¡BLOQUEADO! </STRONG> <BR>' . PHP_EOL;	
		echo 'hora límite: ' . $semaforo->limite .'<br>' . PHP_EOL;
		echo 'contador: ' . $semaforo->contador() . ' accesos' .'<br>' . PHP_EOL;;
		die ();
	}
		
	$semaforo->encender ();
	
	echo 'Límite del semáforo: ' . $semaforo->limite .'<br>' . PHP_EOL;
	echo 'Solo permitirá: ' . $semaforo->contador() . ' accesos' .'<br>' . PHP_EOL;;


	
	
	
	
	
	
	
	
	
	
	
	
	
### Ejemplo con contador, más complejo
Permite hasta 3 conexiones en 5 segundos.
Si cruza el límite establece un bloqueo de 30 segundos.
Se debe actualizar la página para ver el funcionamiento.
	
	require_once ('semaforo.php');
	
	

	$semaforo = new Semaforo ();
	$semaforo->duracion = 5;
	$semaforo->ruta = '';
	$semaforo->contador (3);

	echo  'Es la hora: ' . date('Y-m-d H:i:s') .'<br>' . PHP_EOL;
	
	if ($semaforo->activo()) {
		echo '<STRONG> ¡BLOQUEADO! </STRONG> <BR>' . PHP_EOL;	
		echo 'hora límite: ' . $semaforo->limite .'<br>' . PHP_EOL;
		echo 'contador: ' . $semaforo->contador() . ' accesos' .'<br>' . PHP_EOL;;
		
				
		if ($semaforo->marca == '') {
			$semaforo->contador(1);
			$semaforo->duracion = 30;
			$semaforo->marca = date('Y-m-d H:i:s');
			$semaforo->reencender ();					
			echo 'Ahora el nuevo límite es de 30 segundos y finalizará: ' . $semaforo->limite .'<br>' . PHP_EOL;
		}
		
		
		
		die ();
	}
	
	$semaforo->marca = '';
	$semaforo->encender ();
	
	echo 'Límite del semáforo: ' . $semaforo->limite .'<br>' . PHP_EOL;
	echo 'Solo permitirá: ' . $semaforo->contador() . ' accesos' .'<br>' . PHP_EOL;;
	
	
	
	
	
	
	
	*/
	

	
	
	
	
	
	
	
	class Semaforo {
				
		public $ruta      = 'semaforos';
		public $extension = 'semaforo';
		public $prefijo   = 'bloqueo';
		public $clave     = '0';
		
		public $duracion  = 5;
		public $limite    = null;
		public $marca     = '';
		
		private $contador  = 1;
		private $contador_original = 1;
		
		private $archivo;
		
		
		public function __construct ($clave = '0000', $duracion = 0) {
			$this->clave    = $clave;
			$this->duracion = $duracion;
		}
		
		public function encender () {
			$ahora  = date('Y-m-d H:i:s');
			
			if ($this->limite == null)
				$this->limite = date('Y-m-d H:i:s', strtotime($ahora . ' ' . $this->duracion . ' second'));
			
			if ($this->contador === null)
				$this->contador = $this->contador_original;
			
			$datos [] = $this->limite;
			$datos [] = $this->contador -1;
			$datos [] = $this->marca;
			
			$this->guardar ($datos);
		}
		
		
		public function apagar () {
			//$this->borrar();
			$this->limite = null;
			$this->contador = null;
		}
		
		
		public function reencender () {
			$this->limite   = null;
			$this->contador = null;
			$this->encender();
		}
		
		
		public function activo () {			
			
			$ahora = date('Y-m-d H:i:s');
			
			$datos =  $this->leer();
			
			
			
			if (isset ($datos[0]))
				if ($datos[0] != null)
					if ($datos[0] != '')
						$this->limite = date('Y-m-d H:i:s', strtotime($datos [0]));
			
			if (isset ($datos[1]))				
				if (is_numeric($datos[1])) 
					$this->contador = $datos [1];
						
					
			if (isset ($datos[2]))									
				$this->marca = $datos [2];
			
			if ($this->limite == null)
				return false;
			
			if ($this->contador > 0) 
				return false;
			
			if ($ahora > $this->limite) {				
				$this->apagar ();
				return false;
			}
			
							
			
			return true;
		}
		
		public function inactivo () {
			return !$this->activo ();
		}
		
		
		
		
		
		
		
		
		
		private function leer () {
			$nombreArchivo = $this->nombre ();
			if (file_exists($nombreArchivo))
				$archivo = fopen($nombreArchivo,'r+');
			else
				$archivo = fopen($nombreArchivo,'w+');
			flock($archivo, LOCK_EX);
			$this->archivo = $archivo;
			
			return fgetcsv ($archivo);
		}
		
		
		private function guardar ($datos = null) {
			$archivo = $this->archivo;
			if ($datos == null)
				return fclose ($archivo);
			rewind ($archivo);
			fputcsv ($archivo, $datos);
			return fclose ($archivo);
		}
			
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		private function borrar () {
			$nombreArchivo = $this->nombre ();
			if (file_exists($nombreArchivo))
				unlink ($nombreArchivo);
		}
		
		private function nombre () {
			$nombreArchivo = $this->prefijo . $this->clave . '.' . $this->extension;
			if ($this->ruta != '')
				$nombreArchivo = $this->ruta . '/' . $nombreArchivo;
			
			return $nombreArchivo;
		}
		
		
		
		public function contador ($total = null) {
			if ($total === null)
				return $this->contador;
			$this->contador_original = $total;
			$this->contador = $total;
		}
		
		
		
		
		
		
		public function bloquear () {
			$this->leer();
		}
		
		public function desbloquear () {
			$this->guardar();
		}
		
	}
	
	
	
	
?>