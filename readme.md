# Semáforo PHP.

Pequeño controlador de concurrencia y temporizador.

	
Este programa permite 
* Tomar decisiones en caso de concurrencia.
* Establecer límites de tiempo.
* Establecer límites de accesos.
* Establecer semáforos personalizados.

Con este programa podrás
* Bloquear el acceso de múltiples usuarios a un segmento de código.
* Detener inundaciones o intentos de accesos.
	





### Crea un semáforo.
	
$semaforo = new Semaforo ();

$semaforo = new Semaforo ('usuario1234');





### Directorio de archivos.
$semaforo->ruta = '';


### Cambia la clave del semáforo.
Esto permite usar un semáforo personalizado.
Por ejemplo un semáforo por usuario, IP, actividad, etc.
$semaforo->clave = 'usuario1234';
	
	
	
	
	
## Concurrencia

### Bloquea el acceso concurrente.
Si otra conexión desea acceder suspende su ejecución y queda a la espera de que se libere el bloqueo.
$semaforo->bloquear ();

### Quita el bloqueo.	
$semaforo->desbloquear();




## Semáforo

### Segundos en los que expirará el semáforo.	
$semaforo->duracion = 10; 	


### Consulta si está activo (Bloqueado).
Devolverá "true" si:
* La consulta no supera el límite del contador.
* La consulta no supera los segundos establecidos.
if ($semaforo->activo())	

### Consulta si no está activo.
if ($semaforo->inactivo())	



### Enciende el semáforo.
$semaforo->encender ();

### Apaga el semáforo. 
Es de uso interno. No debería usarse.
$semaforo->apagar ();




## Otros métodos 

### Contador
cantidad de veces que permite pasar el semáforo antes de activarse.
$semaforo->contador (5);


### Apaga y vuelve a encender.
$semaforo->reencender();




	
	
## Ejemplos
	
	
	
	


###  Ejemplo Concurrencia 

Requiere dos instancias ejecutando la misma página al mismo tiempo.
	
	
	require_once ('semaforo.php');

	$semaforo = new Semaforo ();	
	$semaforo->ruta = '';
	

	echo  'Inicio del programa: ' 
		. date('Y-m-d H:i:s') 
		.'<br>' . PHP_EOL;
		
	$semaforo->bloquear ();
	
	echo  'Inicio del código protegido: ' 
		. date('Y-m-d H:i:s') 
		.'<br>' . PHP_EOL;
		
	sleep (5);
	$semaforo->desbloquear();
	
	echo  'Fin del programa: ' 
		. date('Y-m-d H:i:s') 
		.'<br>' . PHP_EOL;
	









### Ejemplo temporizador

Permite una conexión cada 5 segundos.
Se debe actualizar a página para ver el funcionamiento.
	
	require_once ('semaforo.php');
	
	$semaforo = new Semaforo ();
	$semaforo->duracion = 5;
	$semaforo->ruta = '';
	

	echo  'Es la hora: ' . date('Y-m-d H:i:s') .'<br>' . PHP_EOL;
	
	if ($semaforo->activo()) {
		echo '<STRONG> ¡BLOQUEADO! </STRONG> <BR>' . PHP_EOL;	
		echo 'hora límite: ' . $semaforo->limite .'<br>' . PHP_EOL;		
	}
		
	$semaforo->encender ();
	
	echo 'Límite del semáforo: ' . $semaforo->limite .'<br>' . PHP_EOL;
	

	
	











	

	
# Proyectos en los que se utiliza este programa

Foto Reloj
https://t.me/relojbot





# Más aplicaciones

Al no tener publicidad este proyecto se mantiene únicamente con donaciones.
Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 



